dnl aclocal.m4 generated automatically by aclocal 1.4

dnl Copyright (C) 1994, 1995-8, 1999 Free Software Foundation, Inc.
dnl This file is free software; the Free Software Foundation
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY, to the extent permitted by law; without
dnl even the implied warranty of MERCHANTABILITY or FITNESS FOR A
dnl PARTICULAR PURPOSE.

# Do all the work for Automake.  This macro actually does too much --
# some checks are only needed if your package does certain things.
# But this isn't really a big deal.

AC_DEFUN(AC_ENABLE_DEBUG,
[
AC_ARG_ENABLE(debug,        [  --enable-debug          enable debug symbols in library],
[
if test "$enableval" = yes; then
  enable_debug=-g;
  AC_SUBST(enable_debug)
fi])
])

AC_DEFUN(AC_ENABLE_OPTIMIZATION,
[
AC_ARG_ENABLE(optimization, [  --enable-optimization   enable optimization],
[
if test "$enableval" = yes; then
  enable_optimization=-O3;
  AC_SUBST(enable_optimization)
fi])
])

